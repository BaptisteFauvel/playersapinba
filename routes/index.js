var express = require('express');
var router = express.Router();
const axios = require('axios');

router.get('/', function (req, res, next) {
    axios.get('https://fr.global.nba.com/stats2/league/playerlist.json?locale=fr')
        .then(response => {
            response.data.payload.players.forEach(player => {
                player.playerProfile.pic = 'https://ak-static.cms.nba.com/wp-content/uploads/headshots/nba/latest/260x190/' + req.params.id + '.png';
            });
            res.json(response.data.payload.players);
        })
        .catch(error => {
            res.json({error: error});
        });

});

router.get('/:id', function (req, res, next) {
    axios.get('https://fr.global.nba.com/stats2/league/playerlist.json?locale=fr')
        .then(response => {
            players = response.data.payload.players;
            players.forEach(player => {
                if (player.playerProfile.playerId === req.params.id) {
                    player.playerProfile.pic = 'https://ak-static.cms.nba.com/wp-content/uploads/headshots/nba/latest/260x190/' + req.params.id + '.png';
                    res.json(player)
                }
            });
        })
        .catch(error => {
            res.json({error: '500'});
        });

});


module.exports = router;
